package com.coffeepoweredcrew.bridge;

public class Demo {
	
	static int highestPowerof2(int n) 
	{ 
	      
	    int p = (int)(Math.log(n) /  
	                  Math.log(3)); 
	    return (int)Math.pow(3, p);  
	} 
	  
	// Driver code 
	public static void main (String[] args)  
	{ 
	    int n = 28; 
	    System.out.println(highestPowerof2(n)); 
	} 

}

class Demo1 {
	
	int b=10;
	
	static class Demo1Inner{
		int a =10;
		
		public void demoInner(){
			//System.out.println(b);
			System.out.println("Inner Class");
		}
	}

}


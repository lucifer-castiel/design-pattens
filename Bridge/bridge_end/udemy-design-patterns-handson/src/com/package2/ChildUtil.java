package com.package2;

import com.package1.ParentUtil;

public class ChildUtil extends ParentUtil{

	public static void main(String[] args) {
		try{
			
			throw new Exc1();
		}catch(Exc0 e){
			System.out.println("here");
		}
		catch(Exception e){
			System.out.println("out");
		}

	}

	public void callStuff() {
		System.out.println(this.doStuff());
		
	}

}

class Exc0 extends Exception{}

class Exc1 extends Exc0{}